package ru.javaschool.service.cardservice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.javaschool.exceptions.client.ClientNotFoundException;
import ru.javaschool.models.bankentity.account.Card;
import ru.javaschool.models.bankentity.creditcard.CardState;
import ru.javaschool.models.users.Client;
import ru.javaschool.repositories.card.CardRepository;
import ru.javaschool.repositories.user.ClientRepository;

import java.sql.Date;
import java.text.DecimalFormat;
import java.time.LocalDate;

@Service
public class CardService {

    private final ClientRepository clientRepository;
    private final CardRepository cardRepository;

    @Autowired
    public CardService(ClientRepository clientRepository, CardRepository cardRepository) {
        this.clientRepository = clientRepository;
        this.cardRepository = cardRepository;
    }

    @Transactional
    public void save(Card card, UserDetails user) {
        final String login = user.getUsername();
        final Client client = clientRepository
                .getClientByGuest_Login(login)
                .orElseThrow(ClientNotFoundException::new);
        client.getCards().add(card);
        card.setOwner(client);
        card.setState(CardState.ACTIVE);
        card.setEndDate(Date.valueOf(LocalDate.now().plusYears(1)));
        cardRepository.save(card);
        clientRepository.save(client);
    }

    @Transactional
    public void addCash(UserDetails user, long cardId, double amount) {
        if (amount <= 0) {
            throw new IllegalArgumentException();
        }

        final Card card = getCard(user, cardId);

        final double balance = card.getBalance() + amount;
        final double parseBalance = Double
                .parseDouble(new DecimalFormat("#0.00")
                        .format(balance).replace(',', '.'));
        card.setBalance(parseBalance);
        cardRepository.save(card);
    }

    @Transactional
    public void withdrawCash(UserDetails user, long cardId, double amount) {
        if (amount <= 0) {
            throw new IllegalArgumentException();
        }

        final Card card = getCard(user, cardId);

        final double balance = card.getBalance() - amount;

        if (balance < 0) {
            throw new IllegalArgumentException();
        }

        final double parseBalance = Double
                .parseDouble(new DecimalFormat("#0.00")
                        .format(balance).replace(',', '.'));
        card.setBalance(parseBalance);
        cardRepository.save(card);
    }

    @Transactional(readOnly = true)
    private Card getCard(UserDetails user, long cardId) {
        final String login = user.getUsername();
        final Client client = clientRepository
                .getClientByGuest_Login(login)
                .orElseThrow(ClientNotFoundException::new);

        return client
                .getCards().stream()
                .filter(val -> val.getId() == cardId)
                .findAny().orElseThrow(IllegalArgumentException::new);
    }
}
