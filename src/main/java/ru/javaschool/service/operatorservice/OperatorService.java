package ru.javaschool.service.operatorservice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.javaschool.models.bankentity.creditcard.CreditCard;
import ru.javaschool.models.bankentity.creditcard.CreditCardApplication;
import ru.javaschool.models.users.Client;
import ru.javaschool.repositories.user.ClientRepository;
import ru.javaschool.repositories.creditcard.CreditCardApplicationRepository;
import ru.javaschool.repositories.creditcard.CreditCardRepository;

import java.util.List;
import java.util.Optional;

@Service
public class OperatorService {

    private final ClientRepository clientRepository;
    private final CreditCardApplicationRepository creditCardApplicationRepository;
    private final CreditCardRepository creditCardRepository;

    @Autowired
    public OperatorService(ClientRepository clientRepository, CreditCardApplicationRepository creditCardApplicationRepository, CreditCardRepository creditCardRepository) {
        this.clientRepository = clientRepository;
        this.creditCardApplicationRepository = creditCardApplicationRepository;
        this.creditCardRepository = creditCardRepository;
    }

    public List<CreditCardApplication> getAllCreditCardApplication() {
        return creditCardApplicationRepository.getAllBy();
    }

    @Transactional
    public void saveCreditCard(Client client, CreditCard card) {
        card.setId(0);
        creditCardRepository.save(card);
        clientRepository.save(client);
    }

    @Transactional(readOnly = true)
    public Optional<Client> getClientById(long id) {
        return clientRepository.getClientById(id);
    }
}
