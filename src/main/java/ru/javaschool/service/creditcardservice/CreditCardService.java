package ru.javaschool.service.creditcardservice;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.javaschool.exceptions.client.ClientNotFoundException;
import ru.javaschool.exceptions.creditcard.CreditCardNotFoundException;
import ru.javaschool.models.bankentity.creditcard.CreditCard;
import ru.javaschool.models.users.Client;
import ru.javaschool.repositories.user.ClientRepository;
import ru.javaschool.repositories.creditcard.CreditCardRepository;

import java.text.DecimalFormat;
import java.util.List;

@Service
public class CreditCardService {

    private final CreditCardRepository creditCardRepository;
    private final ClientRepository clientRepository;

    public CreditCardService(CreditCardRepository creditCardRepository, ClientRepository clientRepository) {
        this.creditCardRepository = creditCardRepository;
        this.clientRepository = clientRepository;
    }

    @Transactional(readOnly = true)
    public List<CreditCard> getCards(UserDetails user) {
        final String login = user.getUsername();
        final Client client = clientRepository
                .getClientByGuest_Login(login)
                .orElseThrow(ClientNotFoundException::new);
        return client.getCreditCards();
    }

    @Transactional
    public void addCash(UserDetails user, long creditCardId, double amount) {
        if (amount <= 0) {
            throw new IllegalArgumentException();
        }
        final String login = user.getUsername();
        final Client client = clientRepository
                .getClientByGuest_Login(login)
                .orElseThrow(ClientNotFoundException::new);

        final CreditCard card = client
                .getCreditCards().stream()
                .filter(val -> val.getId() == creditCardId)
                .findAny().orElseThrow(CreditCardNotFoundException::new);

        final double balance = card.getBalance() + amount;
        final double parseBalance = Double.parseDouble(
                new DecimalFormat("#0.00").format(balance).replace(',', '.')
        );
        card.setBalance(parseBalance);
        creditCardRepository.save(card);
    }
}
