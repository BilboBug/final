package ru.javaschool.service.creditcardservice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.javaschool.models.bankentity.creditcard.CreditCardApplication;
import ru.javaschool.models.users.Client;
import ru.javaschool.repositories.creditcard.CreditCardApplicationRepository;

@Service
public class CreditCardApplicationService {

    private final CreditCardApplicationRepository creditCardApplicationRepository;

    @Autowired
    public CreditCardApplicationService(CreditCardApplicationRepository creditCardApplicationRepository) {
        this.creditCardApplicationRepository = creditCardApplicationRepository;
    }

    @Transactional
    public void createApp(Client client) {
        final CreditCardApplication creditCardApplication = new CreditCardApplication();
        creditCardApplication.setClient(client);
        creditCardApplicationRepository.save(creditCardApplication);
    }
}
