package ru.javaschool.service.currency;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.javaschool.models.bankentity.currency.Currency;
import ru.javaschool.models.bankentity.currency.CurrencyCommission;
import ru.javaschool.repositories.currency.CurrencyCommissionRepository;

@Service
public class CurrencyCommissionService {

    private final CurrencyCommissionRepository currencyCommissionRepository;

    public CurrencyCommissionService(CurrencyCommissionRepository currencyCommissionRepository) {
        this.currencyCommissionRepository = currencyCommissionRepository;
    }

    @Transactional
    public void setCurrencyCommission(Currency currency, double value) {

        final CurrencyCommission commission = currencyCommissionRepository
                .getFirstByCurrency(currency)
                .orElseThrow(IllegalArgumentException::new);

        commission.setCommission(value);
        currencyCommissionRepository.save(commission);
    }
}
