package ru.javaschool.service.bankaccountservice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.javaschool.exceptions.client.ClientNotFoundException;
import ru.javaschool.exceptions.creditcard.InsufficientFundsException;
import ru.javaschool.models.bankentity.account.BankAccount;
import ru.javaschool.models.users.Client;
import ru.javaschool.repositories.bankaccount.BankAccountRepository;
import ru.javaschool.repositories.user.ClientRepository;

import java.text.DecimalFormat;

@Service
public class BankAccountService {

    private final BankAccountRepository bankAccountRepository;
    private final ClientRepository clientRepository;

    @Autowired
    public BankAccountService(BankAccountRepository bankAccountRepository, ClientRepository clientRepository) {
        this.bankAccountRepository = bankAccountRepository;
        this.clientRepository = clientRepository;
    }

    @Transactional
    public void save(BankAccount account, UserDetails user) {
        final String login = user.getUsername();
        final Client client = clientRepository
                .getClientByGuest_Login(login)
                .orElseThrow(ClientNotFoundException::new);
        account.setId(0);
        account.setOwner(client);

        client.getBankAccounts().add(account);
        bankAccountRepository.save(account);
        clientRepository.save(client);
    }

    @Transactional
    public void addCash(UserDetails user, long bankAccountId, double amount) {
        if (amount <= 0) {
            throw new IllegalArgumentException();
        }

        final BankAccount bankAccount = getBankAccount(user, bankAccountId);

        final double balance = bankAccount.getAmount() + amount;

        final double parseBalance = Double
                .parseDouble(new DecimalFormat("#0.00")
                        .format(balance).replace(',', '.'));

        bankAccount.setAmount(parseBalance);
        bankAccountRepository.save(bankAccount);
    }

    @Transactional
    public void withdrawCash(UserDetails user, long bankAccountId, double amount) {
        if (amount <= 0) {
            throw new IllegalArgumentException();
        }

        final BankAccount bankAccount = getBankAccount(user, bankAccountId);

        final double balance = bankAccount.getAmount() - amount;

        if (balance < 0) {
            throw new InsufficientFundsException();
        }

        final double parseBalance = Double
                .parseDouble(new DecimalFormat("#0.00")
                        .format(balance).replace(',', '.'));

        bankAccount.setAmount(parseBalance);
        bankAccountRepository.save(bankAccount);
    }

    private BankAccount getBankAccount(UserDetails user, long bankAccountId) {
        final String login = user.getUsername();
        final Client client = clientRepository
                .getClientByGuest_Login(login)
                .orElseThrow(ClientNotFoundException::new);

        return client
                .getBankAccounts().stream()
                .filter(val -> val.getId() == bankAccountId)
                .findAny().orElseThrow(IllegalArgumentException::new);
    }

}
