package ru.javaschool.service.creditservice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.javaschool.exceptions.client.ClientNotFoundException;
import ru.javaschool.exceptions.creditcard.CreditCardNotFoundException;
import ru.javaschool.exceptions.creditcard.InsufficientFundsException;
import ru.javaschool.models.bankentity.credit.Credit;
import ru.javaschool.models.bankentity.creditcard.CreditCard;
import ru.javaschool.models.users.Client;
import ru.javaschool.repositories.credit.CreditRepository;
import ru.javaschool.repositories.creditcard.CreditCardRepository;
import ru.javaschool.repositories.user.ClientRepository;

import java.text.DecimalFormat;
import java.util.List;

@Service
public class CreditService {

    private final CreditRepository creditRepository;
    private final ClientRepository clientRepository;
    private final CreditCardRepository creditCardRepository;

    @Autowired
    public CreditService(CreditRepository creditRepository,
                         ClientRepository clientRepository,
                         CreditCardRepository creditCardRepository) {

        this.creditRepository = creditRepository;
        this.clientRepository = clientRepository;
        this.creditCardRepository = creditCardRepository;
    }

    @Transactional(readOnly = true)
    public List<Credit> getCredits(UserDetails user, long creditCardId) {
        final String login = user.getUsername();
        final Client client = clientRepository
                .getClientByGuest_Login(login)
                .orElseThrow(ClientNotFoundException::new);
        client.getCreditCards().stream()
                .filter(value -> value.getId() == creditCardId)
                .findAny().orElseThrow(CreditCardNotFoundException::new);
        return creditRepository.getAllByCreditCard_Id(creditCardId);
    }

    @Transactional
    public void repayCredit(UserDetails user, long creditId, long creditCardId, double amount) {

        final CreditCard card = getCreditCard(user, creditCardId);

        validateAmount(amount, card);

        final Credit credit = getCredit(card, creditId);

        if (amount > credit.getAmount()) {
            throw new IllegalArgumentException();
        }

        double creditAmount = credit.getAmount() - amount;
        double cardBalance = card.getBalance() - amount;

        credit.setAmount(roundAmount(creditAmount));
        card.setBalance(roundAmount(cardBalance));

        if (credit.getAmount() == 0 && credit.getFine() == 0) {
            creditRepository.delete(credit);
        } else {
            creditRepository.save(credit);
        }
        creditCardRepository.save(card);
    }

    @Transactional
    public void repayFines(UserDetails user, long creditCardId, long creditId, double amount) {

        final CreditCard card = getCreditCard(user, creditCardId);

        validateAmount(amount, card);

        final Credit credit = getCredit(card, creditId);

        if (amount > credit.getFine()) {
            throw new IllegalArgumentException();
        }

        card.setBalance(roundAmount(card.getBalance() - amount));
        credit.setFine(roundAmount(credit.getFine() - amount));

        if (credit.getAmount() == 0 && credit.getFine() == 0) {
            creditRepository.delete(credit);
        } else {
            creditRepository.save(credit);
        }
        creditCardRepository.save(card);
    }

    @Transactional(readOnly = true)
    private CreditCard getCreditCard(UserDetails user, long creditCardId) {

        final String login = user.getUsername();
        final Client client = clientRepository
                .getClientByGuest_Login(login)
                .orElseThrow(ClientNotFoundException::new);

        return client.getCreditCards().stream()
                .filter(val -> val.getId() == creditCardId)
                .findAny().orElseThrow(CreditCardNotFoundException::new);
    }

    private Credit getCredit(CreditCard card, long creditId) {

        return card.getCredits().stream()
                .filter(val -> val.getId() == creditId)
                .findAny().orElseThrow(CreditCardNotFoundException::new);
    }

    private void validateAmount(double amount, CreditCard card) {
        if (amount < 0) {
            throw new IllegalArgumentException();
        }

        if (card.getBalance() < amount) {
            throw new InsufficientFundsException();
        }

    }

    private double roundAmount(double amount) {
        return Double.parseDouble(new DecimalFormat("#0.00")
                .format(amount)
                .replace(',', '.'));
    }
}
