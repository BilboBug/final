package ru.javaschool.service.creditservice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.javaschool.exceptions.client.ClientNotFoundException;
import ru.javaschool.exceptions.creditcard.CreditCardNotFoundException;
import ru.javaschool.models.bankentity.credit.Credit;
import ru.javaschool.models.bankentity.creditcard.CreditCard;
import ru.javaschool.models.bankentity.credit.CreditState;
import ru.javaschool.models.users.Client;
import ru.javaschool.repositories.user.ClientRepository;
import ru.javaschool.repositories.creditcard.CreditCardRepository;
import ru.javaschool.repositories.credit.CreditRepository;
import ru.javaschool.util.credit.CreditAmountService;
import ru.javaschool.util.credit.CreditDateService;

import java.sql.Date;
import java.text.DecimalFormat;

@Service
public class OpenCreditService {

    private final ClientRepository clientRepository;
    private final CreditRepository creditRepository;
    private final CreditDateService dateService;
    private final CreditAmountService amountService;
    private final CreditCardRepository cardRepository;

    @Autowired
    public OpenCreditService(ClientRepository clientRepository,
                             CreditRepository creditRepository,
                             CreditDateService dateService,
                             CreditAmountService amountService, CreditCardRepository cardRepository) {

        this.clientRepository = clientRepository;
        this.creditRepository = creditRepository;
        this.dateService = dateService;
        this.amountService = amountService;
        this.cardRepository = cardRepository;
    }

    @Transactional(readOnly = true)
    public Credit getCreditForm(UserDetails user, long creditCardId) {
        final String login = user.getUsername();
        final Client client = clientRepository
                .getClientByGuest_Login(login)
                .orElseThrow(ClientNotFoundException::new);

        if (client.getCreditCards().stream().noneMatch(val -> val.getId() == creditCardId)) {
            throw new CreditCardNotFoundException();
        }

        return new Credit();
    }

    @Transactional
    public void openCredit(UserDetails user, Credit credit, long creditCardId) {
        final String login = user.getUsername();
        final Client client = clientRepository
                .getClientByGuest_Login(login)
                .orElseThrow(ClientNotFoundException::new);

        final CreditCard card = client.getCreditCards().stream()
                .filter((value) -> value.getId() == creditCardId)
                .findAny().orElseThrow(IllegalArgumentException::new);

        credit.setCreditCard(card);
        credit.setCreditState(CreditState.ACTIVE);
        card.getCredits().add(credit);

        final double amount = amountService.convertSumToRub(credit.getAmount(), credit.getCurrency());
        final double commission = amountService.getCommission(credit.getCurrency());
        final Date date = dateService.computeCreditEndDate(card.getEndDate());
        final double creditAmount = amount + amount * commission + amount * card.getInterestRate();

        amountService.validateAmount(creditAmount, card);

        final double parseCreditAmount = Double
                .parseDouble(new DecimalFormat("#0.00")
                        .format(creditAmount)
                        .replace(',', '.'));

        final double parseAmount = Double
                .parseDouble(new DecimalFormat("#0.00")
                        .format(amount)
                        .replace(',', '.'));

        credit.setAmount(parseCreditAmount);
        credit.setEndDate(date);

        card.setBalance(card.getBalance() + parseAmount);

        creditRepository.save(credit);
        cardRepository.save(card);
    }

}
