package ru.javaschool.service.creditservice;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.javaschool.models.bankentity.credit.Fine;
import ru.javaschool.repositories.credit.FineRepository;

@Service
public class FinesService {

    private final FineRepository fineRepository;

    public FinesService(FineRepository fineRepository) {
        this.fineRepository = fineRepository;
    }

    @Transactional
    public void setFine(double value) {
        if (value < 0) {
            throw new IllegalArgumentException();
        }
        final Fine fine = fineRepository.getOne(1L);
        fine.setValue(value);
        fineRepository.save(fine);
    }
}
