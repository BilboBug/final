package ru.javaschool.service.transferservice;

import ru.javaschool.models.bankentity.transfer.Transfer;
import ru.javaschool.models.users.Client;

public interface TransferExecutor {
    void executeTransfer(Client client, Transfer transfer);
}
