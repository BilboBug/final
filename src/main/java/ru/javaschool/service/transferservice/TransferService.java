package ru.javaschool.service.transferservice;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.javaschool.exceptions.client.ClientNotFoundException;
import ru.javaschool.models.bankentity.transfer.Transfer;
import ru.javaschool.models.users.Client;
import ru.javaschool.repositories.transfer.TransferRepository;
import ru.javaschool.repositories.user.ClientRepository;

import java.util.List;

@Service
public class TransferService {

    private final TransferRepository transferRepository;
    private final ClientRepository clientRepository;

    public TransferService(TransferRepository transferRepository, ClientRepository clientRepository) {
        this.transferRepository = transferRepository;
        this.clientRepository = clientRepository;
    }

    @Transactional(readOnly = true)
    public List<Transfer> getTransferHistory(UserDetails user) {
        final String login = user.getUsername();
        final Client client = clientRepository
                .getClientByGuest_Login(login)
                .orElseThrow(ClientNotFoundException::new);

        return client.getTransfers();
    }
}
