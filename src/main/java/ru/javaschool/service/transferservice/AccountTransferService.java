package ru.javaschool.service.transferservice;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.javaschool.models.bankentity.account.BankAccount;
import ru.javaschool.models.bankentity.transfer.Transfer;
import ru.javaschool.models.bankentity.transfer.TransferType;
import ru.javaschool.models.users.Client;
import ru.javaschool.repositories.bankaccount.BankAccountRepository;
import ru.javaschool.repositories.transfer.TransferRepository;
import ru.javaschool.repositories.user.ClientRepository;
import ru.javaschool.util.transfer.TransferValidateService;

import java.text.DecimalFormat;
import java.util.Date;

@Service
public class AccountTransferService implements TransferExecutor {

    private final TransferRepository transferRepository;
    private final ClientRepository clientRepository;
    private final BankAccountRepository bankAccountRepository;
    private final TransferValidateService validateService;

    public AccountTransferService(TransferRepository transferRepository,
                                  ClientRepository clientRepository,
                                  BankAccountRepository bankAccountRepository,
                                  TransferValidateService validateService) {

        this.transferRepository = transferRepository;
        this.clientRepository = clientRepository;
        this.bankAccountRepository = bankAccountRepository;
        this.validateService = validateService;
    }

    @Transactional
    public void executeTransfer(Client client, Transfer transfer) {

        final BankAccount bankAccountFrom = client
                .getBankAccounts().stream()
                .filter(val -> val.getAccountNumber().equals(transfer.getAccountNumberFrom()))
                .findAny().orElseThrow(IllegalArgumentException::new);

        final BankAccount bankAccountTo = client
                .getBankAccounts().stream()
                .filter(val -> val.getAccountNumber().equals(transfer.getAccountNumberTo()))
                .findAny().orElseThrow(IllegalArgumentException::new);

        validateService.validate(bankAccountFrom.getAmount(), transfer.getAmount());

        final DecimalFormat format = new DecimalFormat("#0.00");
        final double fromBalance = bankAccountFrom.getAmount() - transfer.getAmount();
        final double parseFrom = Double.parseDouble(format.format(fromBalance).replace(',', '.'));

        final double toBalance = bankAccountTo.getAmount() + transfer.getAmount();
        final double parseTo = Double.parseDouble(format.format(toBalance).replace(',', '.'));

        bankAccountFrom.setAmount(parseFrom);
        bankAccountTo.setAmount(parseTo);

        transfer.setTransferType(TransferType.ACCOUNT);
        transfer.setDate(new Date(System.currentTimeMillis()));

        client.getTransfers().add(transfer);
        transferRepository.save(transfer);
        bankAccountRepository.save(bankAccountFrom);
        bankAccountRepository.save(bankAccountTo);
        clientRepository.save(client);
    }
}
