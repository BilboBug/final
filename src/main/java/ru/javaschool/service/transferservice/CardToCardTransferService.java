package ru.javaschool.service.transferservice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.javaschool.models.bankentity.account.Card;
import ru.javaschool.models.bankentity.transfer.Transfer;
import ru.javaschool.models.bankentity.transfer.TransferState;
import ru.javaschool.models.bankentity.transfer.TransferType;
import ru.javaschool.models.users.Client;
import ru.javaschool.repositories.card.CardRepository;
import ru.javaschool.repositories.transfer.TransferRepository;
import ru.javaschool.repositories.user.ClientRepository;
import ru.javaschool.util.transfer.TransferValidateService;
import ru.javaschool.util.transfer.TransferCommissionService;

import java.text.DecimalFormat;
import java.util.Date;
import java.util.List;

@Service
public class CardToCardTransferService implements TransferExecutor {

    private final ClientRepository clientRepository;
    private final TransferRepository transferRepository;
    private final CardRepository cardRepository;
    private final TransferValidateService transferValidateService;
    private final TransferCommissionService transferCommissionService;

    @Autowired
    public CardToCardTransferService(ClientRepository clientRepository,
                                     TransferRepository transferRepository,
                                     CardRepository cardRepository,
                                     TransferValidateService transferValidateService,
                                     TransferCommissionService transferCommissionService) {

        this.clientRepository = clientRepository;
        this.transferRepository = transferRepository;
        this.cardRepository = cardRepository;
        this.transferValidateService = transferValidateService;
        this.transferCommissionService = transferCommissionService;
    }

    @Override
    @Transactional
    public void executeTransfer(Client client, Transfer transfer) {
        final double commission = transferCommissionService.getCommission(transfer);

        final Card cardFrom = client.getCards().stream()
                .filter(val -> val.getCardNumber().equals(transfer.getAccountNumberFrom()))
                .findAny().orElseThrow(IllegalArgumentException::new);

        final Card cardTo = cardRepository
                .getCardByCardNumber(transfer.getAccountNumberTo())
                .orElseThrow(IllegalArgumentException::new);

        transfer.setOwner(client);
        transfer.setTransferType(TransferType.CARD_TO_CARD);

        transferValidateService.fraudCheck(transfer);
        transferValidateService.validate(cardFrom.getBalance(), transfer.getAmount() + transfer.getAmount() * commission);

        final double cardFromBalance = cardFrom.getBalance() - transfer.getAmount() - transfer.getAmount() * commission;
        final double parseFrom = Double
                .parseDouble(new DecimalFormat("#0.00")
                        .format(cardFromBalance).replace(',', '.'));

        final double cardToBalance = cardTo.getBalance() + transfer.getAmount();
        final double parseTo = Double
                .parseDouble(new DecimalFormat("#0.00")
                        .format(cardToBalance).replace(',', '.'));

        cardFrom.setBalance(parseFrom);
        cardTo.setBalance(parseTo);

        client.getTransfers().add(transfer);

        transfer.setDate(new Date(System.currentTimeMillis()));

        transferRepository.save(transfer);
        cardRepository.save(cardFrom);
        cardRepository.save(cardTo);
        clientRepository.save(client);
    }

    public List<Transfer> getBlockedTransfers() {
        return transferRepository.getAllByState(TransferState.BLOCKED);
    }
}
