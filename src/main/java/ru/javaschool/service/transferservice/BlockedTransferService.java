package ru.javaschool.service.transferservice;

import org.springframework.stereotype.Service;
import ru.javaschool.models.bankentity.transfer.Transfer;
import ru.javaschool.models.bankentity.transfer.TransferState;
import ru.javaschool.repositories.transfer.TransferRepository;

import java.util.List;

@Service
public class BlockedTransferService {

    private final CardToCardTransferService cardToCardTransferService;
    private final TransferRepository transferRepository;

    public BlockedTransferService(CardToCardTransferService cardToCardTransferService, TransferRepository transferRepository) {
        this.cardToCardTransferService = cardToCardTransferService;
        this.transferRepository = transferRepository;
    }

    public List<Transfer> getBlockedTransfers() {
        return cardToCardTransferService.getBlockedTransfers();
    }

    public void approveTransfer(long transferId) {
        final Transfer transfer = transferRepository.getOne(transferId);
        cardToCardTransferService.executeTransfer(transfer.getOwner(), transfer);
    }

    public void deleteTransfer(long transferId) {
        final Transfer transfer = transferRepository.getOne(transferId);
        if (!transfer.getState().equals(TransferState.BLOCKED)) {
            throw new IllegalArgumentException();
        }
        transferRepository.delete(transfer);
    }
}
