package ru.javaschool.service.transferservice;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.javaschool.models.bankentity.account.BankAccount;
import ru.javaschool.models.bankentity.account.Card;
import ru.javaschool.models.bankentity.transfer.Transfer;
import ru.javaschool.models.bankentity.transfer.TransferType;
import ru.javaschool.models.users.Client;
import ru.javaschool.repositories.bankaccount.BankAccountRepository;
import ru.javaschool.repositories.card.CardRepository;
import ru.javaschool.repositories.transfer.TransferRepository;
import ru.javaschool.repositories.user.ClientRepository;
import ru.javaschool.util.transfer.TransferValidateService;

import java.text.DecimalFormat;
import java.util.Date;

@Service
public class CardToAccountTransferService implements TransferExecutor {

    private final TransferRepository transferRepository;
    private final ClientRepository clientRepository;
    private final BankAccountRepository bankAccountRepository;
    private final CardRepository cardRepository;
    private final TransferValidateService validateService;

    public CardToAccountTransferService(TransferRepository transferRepository,
                                        ClientRepository clientRepository,
                                        BankAccountRepository bankAccountRepository,
                                        CardRepository cardRepository,
                                        TransferValidateService validateService) {

        this.transferRepository = transferRepository;
        this.clientRepository = clientRepository;
        this.bankAccountRepository = bankAccountRepository;
        this.cardRepository = cardRepository;
        this.validateService = validateService;
    }

    @Transactional
    public void executeTransfer(Client client, Transfer transfer) {

        final Card cardFrom = client
                .getCards().stream()
                .filter(val -> val.getCardNumber().equals(transfer.getAccountNumberFrom()))
                .findAny().orElseThrow(IllegalArgumentException::new);

        final BankAccount accountTo = client
                .getBankAccounts().stream()
                .filter(val -> val.getAccountNumber().equals(transfer.getAccountNumberTo()))
                .findAny().orElseThrow(IllegalArgumentException::new);

        validateService.validate(cardFrom.getBalance(), transfer.getAmount());

        transfer.setTransferType(TransferType.CARD_TO_ACCOUNT);
        transfer.setOwner(client);
        client.getTransfers().add(transfer);

        final DecimalFormat format = new DecimalFormat("#0.00");
        final double fromBalance = cardFrom.getBalance() - transfer.getAmount();
        final double parseFrom = Double.parseDouble(format.format(fromBalance).replace(',', '.'));

        final double toBalance = accountTo.getAmount() + transfer.getAmount();
        final double parseTo = Double.parseDouble(format.format(toBalance).replace(',', '.'));

        cardFrom.setBalance(parseFrom);
        accountTo.setAmount(parseTo);

        transfer.setDate(new Date(System.currentTimeMillis()));

        transferRepository.save(transfer);
        bankAccountRepository.save(accountTo);
        cardRepository.save(cardFrom);
        clientRepository.save(client);
    }
}
