package ru.javaschool.service.contributionservice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.javaschool.exceptions.client.ClientNotFoundException;
import ru.javaschool.models.bankentity.contribution.Contribution;
import ru.javaschool.models.bankentity.contribution.Tariff;
import ru.javaschool.models.users.Client;
import ru.javaschool.repositories.contribution.ContributionRepository;
import ru.javaschool.repositories.contribution.TariffRepository;
import ru.javaschool.repositories.user.ClientRepository;
import ru.javaschool.util.contribution.ContributionValidator;

import java.sql.Date;
import java.text.DecimalFormat;
import java.util.List;

@Service
public class ContributionService {

    private final ContributionRepository contributionRepository;
    private final ClientRepository clientRepository;
    private final ContributionValidator validator;
    private final TariffRepository tariffRepository;

    @Autowired
    public ContributionService(ContributionRepository contributionRepository,
                               ClientRepository clientRepository,
                               ContributionValidator validator,
                               TariffRepository tariffRepository) {

        this.contributionRepository = contributionRepository;
        this.clientRepository = clientRepository;
        this.validator = validator;
        this.tariffRepository = tariffRepository;
    }

    @Transactional
    public void openContribution(UserDetails user, long tariffId, double amount) {
        final String login = user.getUsername();
        final Client client = clientRepository
                .getClientByGuest_Login(login)
                .orElseThrow(ClientNotFoundException::new);

        final Tariff tariff = tariffRepository
                .getTariffById(tariffId)
                .orElseThrow(IllegalArgumentException::new);

        validator.validateMinContributionAmount(tariff, amount);
        validator.validateMaxContributionAmount(tariff, amount);

        final Contribution contribution = new Contribution();
        contribution.setAmount(amount);
        contribution.setOwner(client);
        contribution.setTariff(tariff);

        final Date currentDate = new Date(System.currentTimeMillis());

        contribution.setOpenDate(currentDate);
        contribution.setEndDate(Date.valueOf(currentDate.toLocalDate().plusMonths(tariff.getDurationInMonths())));
        contribution.setNextCapitalizationDate(Date.valueOf(currentDate.toLocalDate().plusMonths(1)));

        client.getContributions().add(contribution);

        contributionRepository.save(contribution);
        clientRepository.save(client);
    }

    @Transactional(readOnly = true)
    public List<Tariff> getAllTariffs() {
        return tariffRepository.getTariffsBy();
    }

    public void addCash(UserDetails user, long contributionId, double amount) {

        if (amount <= 0) {
            throw new IllegalArgumentException();
        }

        final String login = user.getUsername();
        final Client client = clientRepository.getClientByGuest_Login(login).orElseThrow(ClientNotFoundException::new);

        final Contribution contribution = client
                .getContributions().stream()
                .filter(val -> val.getId() == contributionId && val.getTariff().isReplenished())
                .findAny().orElseThrow(IllegalArgumentException::new);

        final double replenishment = contribution.getReplenishmentInMonth() + amount;

        validator.validateMaxContributionAmount(contribution.getTariff(), replenishment + contribution.getAmount());
        final double parseBalance = Double
                .parseDouble(new DecimalFormat("#0.00")
                        .format(replenishment).replace(',', '.'));

        contribution.setReplenishmentInMonth(parseBalance);
        contributionRepository.save(contribution);
    }

    @Transactional
    public void withdrawCash(UserDetails user, long contributionId, double amount) {

        if (amount < 0) {
            throw new IllegalArgumentException();
        }

        final String login = user.getUsername();
        final Client client = clientRepository.getClientByGuest_Login(login).orElseThrow(ClientNotFoundException::new);

        final Contribution contribution = client
                .getContributions().stream()
                .filter(val -> val.getId() == contributionId && val.getTariff().isWithdrawAvailable())
                .findAny().orElseThrow(IllegalArgumentException::new);

        final double balance = contribution.getAmount() - amount;

        validator.validateMinContributionAmount(contribution.getTariff(), balance);

        final double parseBalance = Double
                .parseDouble(new DecimalFormat("#0.00")
                        .format(balance).replace(',', '.'));

        contribution.setAmount(parseBalance);
        contributionRepository.save(contribution);
    }

    @Transactional
    public void closeContribution(UserDetails user, long contributionId) {
        final String login = user.getUsername();
        final Client client = clientRepository
                .getClientByGuest_Login(login)
                .orElseThrow(ClientNotFoundException::new);

        final Contribution contribution = client
                .getContributions().stream()
                .filter(val -> val.getId() == contributionId && val.getTariff().isEarlyClosing())
                .findAny().orElseThrow(IllegalArgumentException::new);

        contributionRepository.delete(contribution);
    }
}
