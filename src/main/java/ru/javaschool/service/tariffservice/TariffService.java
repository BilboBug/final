package ru.javaschool.service.tariffservice;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.javaschool.models.bankentity.contribution.Tariff;
import ru.javaschool.repositories.contribution.TariffRepository;
import ru.javaschool.util.tariff.TariffValidateService;

import java.util.List;

@Service
public class TariffService {

    private final TariffRepository tariffRepository;
    private final TariffValidateService validateService;

    public TariffService(TariffRepository tariffRepository, TariffValidateService validateService) {
        this.tariffRepository = tariffRepository;
        this.validateService = validateService;
    }

    public List<Tariff> getTariffs() {
        return tariffRepository.getTariffsBy();
    }

    @Transactional
    public void updateTariff(Tariff tariff) {
        validateService.validate(tariff);
        tariffRepository.save(tariff);
    }

    @Transactional
    public void createTariff(Tariff tariff) {
        validateService.validate(tariff);
        tariff.setId(0);
        tariffRepository.save(tariff);
    }

    @Transactional
    public void deleteTariff(Tariff tariff) {
        tariffRepository.delete(tariff);
    }

    @Transactional(readOnly = true)
    public Tariff getTariff(long tariffId) {
        return tariffRepository.getOne(tariffId);
    }
}
