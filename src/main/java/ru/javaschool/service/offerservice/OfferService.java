package ru.javaschool.service.offerservice;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.javaschool.exceptions.client.ClientNotFoundException;
import ru.javaschool.models.bankentity.creditcard.CardState;
import ru.javaschool.models.bankentity.creditcard.CreditCard;
import ru.javaschool.models.bankentity.creditcard.CreditCardApplication;
import ru.javaschool.models.users.Client;
import ru.javaschool.repositories.user.ClientRepository;
import ru.javaschool.repositories.creditcard.CreditCardApplicationRepository;
import ru.javaschool.repositories.creditcard.CreditCardRepository;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class OfferService {

    private final ClientRepository clientRepository;
    private final CreditCardRepository creditCardRepository;
    private final CreditCardApplicationRepository applicationRepository;

    public OfferService(ClientRepository clientRepository, CreditCardRepository creditCardRepository, CreditCardApplicationRepository applicationRepository) {
        this.clientRepository = clientRepository;
        this.creditCardRepository = creditCardRepository;
        this.applicationRepository = applicationRepository;
    }

    @Transactional(readOnly = true)
    public List<CreditCard> getOffers(UserDetails user) {
        final String login = user.getUsername();
        final Client client = clientRepository
                .getClientByGuest_Login(login)
                .orElseThrow(ClientNotFoundException::new);
        final List<CreditCard> offers = client.getCreditCards().stream()
                .filter((val) -> val.getState().equals(CardState.CONSIDER))
                .collect(Collectors.toList());
        return offers;
    }

    @Transactional
    public void acceptOffer(UserDetails user, long offerId) {
        final String login = user.getUsername();
        final Client client = clientRepository
                .getClientByGuest_Login(login)
                .orElseThrow(ClientNotFoundException::new);
        final CreditCard card = client.getCreditCards()
                .stream().filter((val) -> val.getId() == offerId)
                .findAny().orElseThrow(IllegalArgumentException::new);
        card.setState(CardState.ACTIVE);
        creditCardRepository.save(card);
        for (CreditCardApplication application : applicationRepository.getAllByClient_Id(client.getId())) {
            applicationRepository.delete(application);
        }
    }

    @Transactional
    public void denyOffer(UserDetails user, long offerId) {
        final String login = user.getUsername();
        final Client client = clientRepository
                .getClientByGuest_Login(login)
                .orElseThrow(ClientNotFoundException::new);
        final CreditCard card = client.getCreditCards().stream()
                .filter((val) -> val.getId() == offerId).findAny()
                .orElseThrow(IllegalArgumentException::new);
        creditCardRepository.delete(card);
    }
}
