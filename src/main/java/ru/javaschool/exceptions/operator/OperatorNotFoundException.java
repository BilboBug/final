package ru.javaschool.exceptions.operator;

public class OperatorNotFoundException extends Exception {
    public OperatorNotFoundException() {
        super();
    }

    public OperatorNotFoundException(String message) {
        super(message);
    }

    public OperatorNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public OperatorNotFoundException(Throwable cause) {
        super(cause);
    }
}
