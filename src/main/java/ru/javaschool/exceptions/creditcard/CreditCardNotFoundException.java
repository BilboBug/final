package ru.javaschool.exceptions.creditcard;

public class CreditCardNotFoundException extends RuntimeException {
    public CreditCardNotFoundException() {
        super();
    }

    public CreditCardNotFoundException(String message) {
        super(message);
    }

    public CreditCardNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public CreditCardNotFoundException(Throwable cause) {
        super(cause);
    }
}
