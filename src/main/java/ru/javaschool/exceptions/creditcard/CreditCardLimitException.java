package ru.javaschool.exceptions.creditcard;

public class CreditCardLimitException extends RuntimeException {
    public CreditCardLimitException() {
    }

    public CreditCardLimitException(String message) {
        super(message);
    }

    public CreditCardLimitException(String message, Throwable cause) {
        super(message, cause);
    }

    public CreditCardLimitException(Throwable cause) {
        super(cause);
    }
}
