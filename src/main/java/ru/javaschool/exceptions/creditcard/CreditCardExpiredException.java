package ru.javaschool.exceptions.creditcard;

public class CreditCardExpiredException extends RuntimeException {
    public CreditCardExpiredException() {
    }

    public CreditCardExpiredException(String message) {
        super(message);
    }

    public CreditCardExpiredException(String message, Throwable cause) {
        super(message, cause);
    }

    public CreditCardExpiredException(Throwable cause) {
        super(cause);
    }
}
