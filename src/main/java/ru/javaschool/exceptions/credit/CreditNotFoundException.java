package ru.javaschool.exceptions.credit;

public class CreditNotFoundException extends RuntimeException {
    public CreditNotFoundException() {
    }

    public CreditNotFoundException(String message) {
        super(message);
    }

    public CreditNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public CreditNotFoundException(Throwable cause) {
        super(cause);
    }
}
