package ru.javaschool.exceptions.transfer;

public class TransferBlockedException extends RuntimeException {
    public TransferBlockedException() {
    }

    public TransferBlockedException(String message) {
        super(message);
    }

    public TransferBlockedException(String message, Throwable cause) {
        super(message, cause);
    }

    public TransferBlockedException(Throwable cause) {
        super(cause);
    }
}
