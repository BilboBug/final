package ru.javaschool.util.credit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.javaschool.exceptions.creditcard.CreditCardExpiredException;
import ru.javaschool.exceptions.creditcard.CreditCardLimitException;
import ru.javaschool.models.bankentity.credit.Credit;
import ru.javaschool.models.bankentity.creditcard.CreditCard;
import ru.javaschool.models.bankentity.credit.CreditState;
import ru.javaschool.models.bankentity.currency.Currency;
import ru.javaschool.repositories.currency.CurrencyCommissionRepository;
import ru.javaschool.util.currency.CurrencyService;

@Component
public class CreditAmountService {

    private final CurrencyService currencyService;
    private final CurrencyCommissionRepository commissionRepository;

    @Autowired
    public CreditAmountService(CurrencyService currencyService, CurrencyCommissionRepository commissionRepository) {
        this.currencyService = currencyService;
        this.commissionRepository = commissionRepository;
    }

    public Double convertSumToRub(double loanAmount, Currency currency) {
        double curse = 1;
        if (!currency.equals(Currency.RUB)) {
            curse = currencyService.getCurrencyCurs(currency);
        }
        return curse * loanAmount;
    }

    public void validateAmount(double creditAmount, CreditCard card) {
        if (creditAmount <= 0) {
            throw new IllegalArgumentException();
        }
        double creditsSum = creditAmount;
        for (Credit credit : card.getCredits()) {
            creditsSum += credit.getAmount();
            if (credit.getCreditState().equals(CreditState.EXPIRED)) {
                throw new CreditCardExpiredException();
            }
        }
        if (creditsSum > card.getCreditLimit()) {
            throw new CreditCardLimitException();
        }
    }

    public double getCommission(Currency currency) {
        return commissionRepository
                .getFirstByCurrency(currency)
                .orElseThrow(IllegalArgumentException::new)
                .getCommission();
    }
}
