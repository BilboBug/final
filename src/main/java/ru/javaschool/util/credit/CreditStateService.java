package ru.javaschool.util.credit;

import org.springframework.stereotype.Service;
import ru.javaschool.models.bankentity.creditcard.CardState;
import ru.javaschool.models.bankentity.credit.Credit;
import ru.javaschool.models.bankentity.creditcard.CreditCard;
import ru.javaschool.models.bankentity.credit.CreditState;
import ru.javaschool.repositories.credit.CreditRepository;

import java.sql.Date;

@Service
public class CreditStateService {

    public final CreditRepository creditRepository;

    public CreditStateService(CreditRepository creditRepository) {
        this.creditRepository = creditRepository;
    }

    public void checkCreditState(Credit credit) {
        if (credit.getEndDate().before(new Date(System.currentTimeMillis()))) {
            credit.setCreditState(CreditState.EXPIRED);
            creditRepository.save(credit);
        }
    }
}
