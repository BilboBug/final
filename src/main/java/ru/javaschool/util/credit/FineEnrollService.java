package ru.javaschool.util.credit;

import org.springframework.stereotype.Service;
import ru.javaschool.models.bankentity.credit.Credit;
import ru.javaschool.models.bankentity.credit.CreditState;
import ru.javaschool.repositories.credit.CreditRepository;
import ru.javaschool.repositories.credit.FineRepository;

import java.text.DecimalFormat;

@Service
public class FineEnrollService {

    private final CreditRepository creditRepository;
    private final FineRepository fineRepository;

    public FineEnrollService(CreditRepository creditRepository, FineRepository fineRepository) {
        this.creditRepository = creditRepository;
        this.fineRepository = fineRepository;
    }

    public void setFine(Credit credit) {
        if (credit.getCreditState().equals(CreditState.EXPIRED) && credit.getFine() < credit.getAmount() * 0.4) {
            final double fine = credit.getFine();
            final double amount = fine + credit.getAmount() * fineRepository.getOne(1L).getValue();
            final double parseAmount = Double.parseDouble(new DecimalFormat("#0.00")
                            .format(amount).replace(',', '.'));

            credit.setFine(parseAmount);
            creditRepository.save(credit);
        }
    }
}
