package ru.javaschool.util.contribution;

import org.springframework.stereotype.Service;
import ru.javaschool.models.bankentity.contribution.Tariff;
import ru.javaschool.models.users.Client;

@Service
public class ContributionValidator {

    public void validateMinContributionAmount(Tariff tariff, double amount) {
        if (tariff.getMinAmount() > amount) {
            throw new IllegalArgumentException();
        }
    }

    public void validateMaxContributionAmount(Tariff tariff, double amount) {
        if (tariff.getMaxAmount() < amount) {
            throw new IllegalArgumentException();
        }
    }
}
