package ru.javaschool.util;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CurrencyBean {
    @JsonProperty("ID")
    private String ID;
    @JsonProperty("NumCode")
    private String numCode;
    @JsonProperty("CharCode")
    private String charCode;
    @JsonProperty("Nominal")
    private String nominal;
    @JsonProperty("Name")
    private String name;
    @JsonProperty("Value")
    private String value;
}
