package ru.javaschool.util.transfer;

import org.springframework.stereotype.Service;
import ru.javaschool.models.bankentity.transfer.Transfer;

@Service
public class TransferCommissionService {

    public double getCommission(Transfer transfer) {
        if (!transfer.getAccountNumberTo().startsWith("666666"))
            return 0.1;
        return 0;
    }
}
