package ru.javaschool.util.transfer;

import org.springframework.stereotype.Service;
import ru.javaschool.exceptions.transfer.TransferBlockedException;
import ru.javaschool.models.bankentity.transfer.Transfer;
import ru.javaschool.models.bankentity.transfer.TransferState;
import ru.javaschool.repositories.transfer.TransferRepository;

@Service
public class TransferValidateService {

    private final TransferRepository transferRepository;

    public TransferValidateService(TransferRepository transferRepository) {
        this.transferRepository = transferRepository;
    }

    public void fraudCheck(Transfer transfer) {
//        if (transfer.getState().equals(TransferState.UNBLOCKED)) {
//            return;
//        }
        if (!transfer.getAccountNumberTo().startsWith("666666") && transfer.getAmount() > 1000) {
            transfer.setState(TransferState.BLOCKED);
            transferRepository.save(transfer);
            throw new TransferBlockedException();
        }
    }

    public void validate(double accountBalance, double transferAmount) {
        if (transferAmount <= 0) {
            throw new IllegalArgumentException();
        }

        if (accountBalance < transferAmount) {
            throw new IllegalArgumentException();
        }
    }
}
