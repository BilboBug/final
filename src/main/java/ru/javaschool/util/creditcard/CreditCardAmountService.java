package ru.javaschool.util.creditcard;

import ru.javaschool.models.bankentity.creditcard.CreditCard;

public class CreditCardAmountService {

    public void validateAmount(CreditCard card, double amount) {
        if (amount <= 0) {
            throw new IllegalArgumentException();
        }
    }
}
