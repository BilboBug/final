package ru.javaschool.util;

import org.springframework.stereotype.Service;
import ru.javaschool.models.bankentity.creditcard.CardState;
import ru.javaschool.models.bankentity.credit.Credit;
import ru.javaschool.models.bankentity.creditcard.CreditCard;
import ru.javaschool.models.bankentity.credit.CreditState;

import java.sql.Date;

@Service
public class CreditStateService {

    public void checkCreditState(Credit credit) {
        if (credit.getEndDate().before(new Date(System.currentTimeMillis()))) {
            credit.setCreditState(CreditState.EXPIRED);
        }
    }

    public void checkCreditCardState(CreditCard creditCard) {
        if (creditCard.getEndDate().before(new Date(System.currentTimeMillis()))) {
            creditCard.setState(CardState.EXPIRED);
        }
    }
}
