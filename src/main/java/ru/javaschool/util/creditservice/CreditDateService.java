package ru.javaschool.util.creditservice;

import org.springframework.stereotype.Component;
import ru.javaschool.exceptions.creditcard.CreditCardExpiredException;

import java.sql.Date;
import java.time.Period;

@Component
public class CreditDateService {

    public Date computeCreditEndDate(Date creditCardEndDate) {
        final Date currentDate = new Date(System.currentTimeMillis());
        final Period period = Period.between(currentDate.toLocalDate(), creditCardEndDate.toLocalDate());
        if (period.getDays() <= 1 || period.isNegative())
            throw new CreditCardExpiredException();
        if (period.getDays() <= 30) {
            return creditCardEndDate;
        } else {
           return Date.valueOf(currentDate.toLocalDate().plusDays(30));
        }
    }
}
