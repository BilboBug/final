package ru.javaschool.util;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import ru.javaschool.models.bankentity.credit.Credit;
import ru.javaschool.models.bankentity.creditcard.CreditCard;
import ru.javaschool.models.bankentity.credit.CreditState;
import ru.javaschool.repositories.creditcard.CreditCardRepository;
import ru.javaschool.repositories.credit.CreditRepository;

import java.sql.Date;
import java.util.List;

@Service
public class CreditScheduler {

    private final CreditCardRepository creditCardRepository;
    private final CreditStateService creditStateService;
    private final FineEnrollService fineEnrollService;
    private final CreditRepository creditRepository;

    public CreditScheduler(CreditCardRepository creditCardRepository,
                           CreditStateService creditStateService,
                           FineEnrollService fineEnrollService,
                           CreditRepository creditRepository) {

        this.creditCardRepository = creditCardRepository;
        this.creditStateService = creditStateService;
        this.fineEnrollService = fineEnrollService;
        this.creditRepository = creditRepository;
    }

    @Scheduled(cron = "0 0 1 * * *")
    public void checkCards() {
        final List<Credit> credits = creditRepository.getAllByEndDateBefore(new Date(System.currentTimeMillis()));
        credits.forEach(val -> {
            creditStateService.checkCreditState(val);
            fineEnrollService.setFine(val);
        });

        final List<CreditCard> creditCards = creditCardRepository.getAllByEndDateBefore(new Date(System.currentTimeMillis()));
        for (CreditCard card: creditCards) {
            if (card.getCredits().stream().noneMatch(val -> val.getCreditState().equals(CreditState.EXPIRED))) {
                creditCardRepository.delete(card);
            }
        }
    }
}
