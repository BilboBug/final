package ru.javaschool.util;

import org.springframework.stereotype.Service;
import ru.javaschool.models.bankentity.credit.Credit;
import ru.javaschool.models.bankentity.credit.CreditState;

@Service
public class FineEnrollService {

    public void setFine(Credit credit) {
        if (credit.getCreditState().equals(CreditState.EXPIRED)) {
            final int fine = credit.getFine();
            credit.setFine(Math.round(Math.round(fine + credit.getAmount() * 0.01)));
        }
    }
}
