package ru.javaschool.util;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.javaschool.models.bankentity.currency.Currency;
import ru.javaschool.models.bankentity.currency.CurrencyCode;
import ru.javaschool.repositories.currency.CurrencyCodeRepository;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class CurrencyService {

    private final CurrencyCodeRepository currencyCodeRepository;

    @Autowired
    public CurrencyService(CurrencyCodeRepository currencyCodeRepository) {
        this.currencyCodeRepository = currencyCodeRepository;
    }

    public double getCurrencyCurs(Currency currency) {
        try {
            final CurrencyCode code = currencyCodeRepository
                    .getFirstByCurrency(currency)
                    .orElseThrow(IllegalArgumentException::new);

            final URL url = new URL("http://www.cbr.ru/scripts/XML_daily.asp");
            final HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");
            try (final BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()))) {
                String inputLine;
                final StringBuilder content = new StringBuilder();
                while ((inputLine = in.readLine()) != null) {
                    content.append(inputLine);
                }
                String xmlAnswer = "";
                final Pattern pattern = Pattern.compile("<Valute ID=\"" + code.getCode() + "\">(.+?)</Valute>");
                final Matcher matcher = pattern.matcher(content.toString());
                while (matcher.find())
                    xmlAnswer = matcher.group();

                XmlMapper xmlMapper = new XmlMapper();
                CurrencyBean value
                        = xmlMapper.readValue(xmlAnswer, CurrencyBean.class);

                return Double.parseDouble(value.getValue().replace(',', '.'));
            } catch (final Exception ex) {
                ex.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return 0;
    }

}
