package ru.javaschool.util.tariff;

import org.springframework.stereotype.Service;
import ru.javaschool.models.bankentity.contribution.Tariff;

@Service
public class TariffValidateService {

    public void validate(Tariff tariff) {
        if (tariff.getMaxAmount() <= 0
                || tariff.getMinAmount() <= 0
                || tariff.getInterestRate() <= 0
                || tariff.getDurationInMonths() <= 0) {
            throw new IllegalArgumentException();
        }

        if (tariff.getMaxAmount() <= tariff.getMinAmount()) {
            throw new IllegalArgumentException();
        }
    }
}
