package ru.javaschool.models.users;

public enum Role {
    OPERATOR,
    CLIENT
}
