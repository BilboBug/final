package ru.javaschool.models.users;

public enum  State {
    ACTIVE, BANNED, DELETED
}
