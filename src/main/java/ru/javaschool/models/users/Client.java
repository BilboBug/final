package ru.javaschool.models.users;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.javaschool.models.bankentity.account.BankAccount;
import ru.javaschool.models.bankentity.account.Card;
import ru.javaschool.models.bankentity.contribution.Contribution;
import ru.javaschool.models.bankentity.transfer.Transfer;
import ru.javaschool.models.bankentity.creditcard.CreditCard;

import javax.persistence.*;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Client {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @OneToOne
    @JoinColumn
    private Passport passport;

    @OneToOne
    @JoinColumn
    private Guest guest;

    @OneToMany
    @JoinColumn
    private List<CreditCard> creditCards;

    @OneToMany
    @JoinColumn
    private List<Card> cards;
    @OneToMany
    @JoinColumn
    private List<BankAccount> bankAccounts;
    @OneToMany
    @JoinColumn
    private List<Transfer> transfers;
    @OneToMany
    @JoinColumn
    private List<Contribution> contributions;
}
