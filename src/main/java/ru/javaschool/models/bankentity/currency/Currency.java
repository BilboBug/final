package ru.javaschool.models.bankentity.currency;

public enum Currency {
    EUR, RUB, USD
}
