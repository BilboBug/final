package ru.javaschool.models.bankentity.contribution;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Tariff {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String name;
    private double minAmount;
    private double maxAmount;
    private long durationInMonths;
    private double interestRate;
    private boolean replenished;
    private boolean earlyClosing;
    private boolean capitalized;
    private boolean withdrawAvailable;
}
