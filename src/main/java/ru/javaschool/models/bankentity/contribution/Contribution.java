package ru.javaschool.models.bankentity.contribution;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.javaschool.models.users.Client;

import javax.persistence.*;
import java.sql.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Contribution {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @ManyToOne
    @JoinColumn
    private Client owner;
    private double amount;
    private double replenishmentInMonth;
    @ManyToOne
    @JoinColumn
    private Tariff tariff;
    private Date endDate;
    private Date openDate;
    private Date nextCapitalizationDate;
}
