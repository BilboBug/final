package ru.javaschool.models.bankentity.transfer;

public enum TransferState {
    ACTIVE, BLOCKED, UNBLOCKED
}
