package ru.javaschool.models.bankentity.transfer;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.javaschool.models.users.Client;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Transfer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String accountNumberFrom;
    private String accountNumberTo;
    @ManyToOne
    @JoinColumn
    private Client owner;
    @Enumerated(EnumType.STRING)
    private TransferType transferType;
    private double amount;
    @Temporal(value = TemporalType.TIMESTAMP)
    private Date date;
    @Enumerated(EnumType.STRING)
    private TransferState state;
}
