package ru.javaschool.models.bankentity.transfer;

public enum TransferType {
    CARD_TO_CARD, CARD_TO_ACCOUNT, ACCOUNT
}
