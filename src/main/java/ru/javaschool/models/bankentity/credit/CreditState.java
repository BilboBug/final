package ru.javaschool.models.bankentity.credit;

public enum CreditState {
    ACTIVE, EXPIRED
}
