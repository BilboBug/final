package ru.javaschool.models.bankentity.account;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.javaschool.models.bankentity.creditcard.CardState;
import ru.javaschool.models.users.Client;

import javax.persistence.*;
import java.sql.Date;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Card {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String cardNumber;
    private double balance;
    @OneToOne
    @JoinColumn
    private Client owner;
    @Enumerated(EnumType.STRING)
    private CardState state;
    private Date endDate;
}