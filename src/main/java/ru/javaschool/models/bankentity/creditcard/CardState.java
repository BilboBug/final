package ru.javaschool.models.bankentity.creditcard;

public enum CardState {
    CONSIDER, ACTIVE, REJECTED, EXPIRED
}
