package ru.javaschool.models.bankentity.creditcard;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.javaschool.models.bankentity.credit.Credit;
import ru.javaschool.models.users.Client;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CreditCard {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private double balance;
    private String cardNumber;
    private int creditLimit;
    private double interestRate;
    @ManyToOne
    @JoinColumn
    private Client owner;
    @OneToMany
    @JoinColumn
    private List<Credit> credits;
    @Enumerated(EnumType.STRING)
    private CardState state;
    private Date endDate;
}
