package ru.javaschool.security.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import ru.javaschool.models.users.Role;
import ru.javaschool.security.details.UserDetailsServiceImpl;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private final UserDetailsServiceImpl service;

    private final PasswordEncoder passwordEncoder;

    private final AuthenticationSuccessHandler authenticationSuccessHandler;

    @Autowired
    public SecurityConfig(UserDetailsServiceImpl service, PasswordEncoder passwordEncoder, UrlByRoleAuthenticationSuccessHandler authenticationSuccessHandler) {
        this.service = service;
        this.passwordEncoder = passwordEncoder;
        this.authenticationSuccessHandler = authenticationSuccessHandler;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
//                .antMatchers("/bankAccount/getAccountForm/*", "/card/getCardForm/*", "/cardCash/addCash/*", "/contribution/getContributionForm/*",
//                        "/contributionCash/addCash/*", "/credit/*", "/offer/getOffers/*", "/openCredit/*", "/creditcardapp/createcardapp/*",
//                        "/creditcard/*", "/transfer/getTransferType/*", "/client/*").hasRole(Role.CLIENT.name())
                .antMatchers("/addclient").permitAll()
                .antMatchers("/addoperator").permitAll()
//                .anyRequest().hasRole(Role.OPERATOR.name())
                .and()
                .formLogin()
                    .loginProcessingUrl("/login")
                    .usernameParameter("login")
                    .successHandler(authenticationSuccessHandler);
        http.csrf().disable();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(service).passwordEncoder(passwordEncoder);
    }
}