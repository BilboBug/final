package ru.javaschool.security.details;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import ru.javaschool.repositories.user.GuestRepository;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    private final GuestRepository guestRepository;

    @Autowired
    public UserDetailsServiceImpl(GuestRepository guestRepository) {
        this.guestRepository = guestRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        return new UserDetailsImpl(guestRepository.getFirstByLogin(s).orElseThrow(IllegalArgumentException::new));
    }
}
