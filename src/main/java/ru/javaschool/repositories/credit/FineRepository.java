package ru.javaschool.repositories.credit;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.javaschool.models.bankentity.credit.Fine;

public interface FineRepository extends JpaRepository<Fine, Long> {
}
