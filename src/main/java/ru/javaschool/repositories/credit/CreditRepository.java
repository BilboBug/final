package ru.javaschool.repositories.credit;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.javaschool.models.bankentity.credit.Credit;

import java.sql.Date;
import java.util.List;
import java.util.Optional;

public interface CreditRepository extends JpaRepository<Credit, Long> {
    Optional<Credit> getFirstById(long id);
    List<Credit> getAllByEndDateBefore(Date now);
    List<Credit> getAllByCreditCard_Id(long creditCardId);
}
