package ru.javaschool.repositories.transfer;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.javaschool.models.bankentity.transfer.Transfer;
import ru.javaschool.models.bankentity.transfer.TransferState;

import java.util.List;

public interface TransferRepository extends JpaRepository<Transfer, Long> {
    List<Transfer> getAllByState(TransferState state);
}
