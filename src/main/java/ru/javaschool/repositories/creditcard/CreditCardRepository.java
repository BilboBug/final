package ru.javaschool.repositories.creditcard;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.javaschool.models.bankentity.creditcard.CreditCard;
import ru.javaschool.models.bankentity.creditcard.CardState;

import java.sql.Date;
import java.util.List;
import java.util.Optional;

@Repository
public interface CreditCardRepository extends JpaRepository<CreditCard, Long> {
    List<CreditCard> getAllByOwner_IdAndState(long id, CardState state);
    Optional<CreditCard> getFirstById(long id);
    Optional<CreditCard> getFirstByIdAndOwner_Id(long cardId, long clientId);
    void deleteCreditCardByIdAndOwner_IdAndState(long creditCardId, long clientId, CardState state);
    Optional<CreditCard> getFirstByOwner_IdAndState(long id, CardState state);
    void deleteCreditCardByOwner_IdAndState(long clientId, CardState state);
    List<CreditCard> getAllByEndDateBefore(Date now);
}
