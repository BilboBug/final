package ru.javaschool.repositories.creditcard;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.javaschool.models.bankentity.creditcard.CreditCardApplication;

import java.util.List;

@Repository
public interface CreditCardApplicationRepository extends JpaRepository<CreditCardApplication, Long> {
    List<CreditCardApplication> getAllBy();
    void deleteAllByClient_Id(long clientId);
    void deleteByClient_Id(long clientId);
    List<CreditCardApplication> getAllByClient_Id(long clientId);
}
