package ru.javaschool.repositories.contribution;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.javaschool.models.bankentity.contribution.Contribution;

import java.sql.Date;
import java.util.List;
import java.util.Optional;

@Repository
public interface ContributionRepository extends JpaRepository<Contribution, Long> {
    Optional<Contribution> getContributionByOwner_Id(long clientId);
    List<Contribution> getAllByTariff_Capitalized(boolean isCapitalized);
    List<Contribution> getAllByNextCapitalizationDate(Date date);
}
