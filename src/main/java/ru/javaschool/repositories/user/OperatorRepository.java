package ru.javaschool.repositories.user;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.javaschool.models.users.Operator;

@Repository
public interface OperatorRepository extends JpaRepository<Operator, Long> {
}
