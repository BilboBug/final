package ru.javaschool.repositories.user;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.javaschool.models.users.Passport;

public interface PassportRepository extends JpaRepository<Passport, Long> {
}
