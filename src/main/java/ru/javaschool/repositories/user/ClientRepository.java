package ru.javaschool.repositories.user;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.javaschool.models.users.Client;

import java.util.Optional;

@Repository
public interface ClientRepository extends JpaRepository<Client, Long> {
    Optional<Client> getClientById(long id);
    Optional<Client> getClientByGuest_Id(long id);
    Optional<Client> getClientByGuest_Login(String login);
}
