package ru.javaschool.repositories.bankaccount;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.javaschool.models.bankentity.account.BankAccount;

import java.util.Optional;

public interface BankAccountRepository extends JpaRepository<BankAccount, Long> {
    Optional<BankAccount> getBankAccountByAccountNumber(String accountNumber);
}
