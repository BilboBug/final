package ru.javaschool.repositories.currency;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.javaschool.models.bankentity.currency.Currency;
import ru.javaschool.models.bankentity.currency.CurrencyCommission;

import java.util.Optional;

@Repository
public interface CurrencyCommissionRepository extends JpaRepository<CurrencyCommission, Long> {

    Optional<CurrencyCommission> getFirstByCurrency(Currency currency);
}
