package ru.javaschool.repositories.currency;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.javaschool.models.bankentity.currency.Currency;
import ru.javaschool.models.bankentity.currency.CurrencyCode;

import java.util.Optional;

@Repository
public interface CurrencyCodeRepository extends JpaRepository<CurrencyCode, Long> {
    Optional<CurrencyCode> getFirstByCurrency(Currency currency);
}
