package ru.javaschool.controllers.creditcard;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.javaschool.models.users.Client;
import ru.javaschool.service.clientservice.ClientService;
import ru.javaschool.service.creditcardservice.CreditCardApplicationService;

@Controller
@RequestMapping("/creditcardapp")
public class CreditCardApplicationController {

    private final ClientService clientService;
    private final CreditCardApplicationService creditCardApplicationService;


    @Autowired
    public CreditCardApplicationController(ClientService clientService, CreditCardApplicationService creditCardApplicationService) {
        this.clientService = clientService;
        this.creditCardApplicationService = creditCardApplicationService;
    }

    @GetMapping("/createcardapp")
    public String createCreditCardApplication(@AuthenticationPrincipal UserDetails userDetails) {
        final Client client = clientService.getClientByLogin(userDetails.getUsername()).orElseThrow(IllegalArgumentException::new);
        creditCardApplicationService.createApp(client);
        return "redirect:/client";
    }
}
