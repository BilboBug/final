package ru.javaschool.controllers.creditcard;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.javaschool.service.clientservice.ClientService;
import ru.javaschool.service.creditcardservice.CreditCardService;

@Controller
@RequestMapping("/creditcard")
public class CreditCardController {

    private final ClientService clientService;
    private final CreditCardService creditCardService;

    @Autowired
    public CreditCardController(ClientService clientService, CreditCardService creditCardService) {
        this.clientService = clientService;
        this.creditCardService = creditCardService;
    }

    @GetMapping("/getCards")
    public String getCreditCards(@AuthenticationPrincipal UserDetails userDetails, Model model) {
        model.addAttribute("cards", creditCardService.getCards(userDetails));
        return "creditcard/cards";
    }

    @GetMapping("/getPaymentForm/{creditCardId}")
    public String getPaymentForm(@PathVariable long creditCardId, Model model) {
        model.addAttribute("creditCardId", creditCardId);
        return "creditcard/cardPayForm";
    }

    @PostMapping("/addCash")
    public String addCash(@AuthenticationPrincipal UserDetails userDetails,
                          @RequestParam long creditCardId,
                          double amount) {

        creditCardService.addCash(userDetails, creditCardId, amount);
        return "redirect:/client";
    }
}
