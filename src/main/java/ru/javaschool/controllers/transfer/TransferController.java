package ru.javaschool.controllers.transfer;

import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.javaschool.models.bankentity.transfer.Transfer;
import ru.javaschool.models.bankentity.transfer.TransferState;
import ru.javaschool.service.transferservice.TransferService;

@Controller
@RequestMapping("/transfer")
public class TransferController {

    private final TransferService transferService;

    public TransferController(TransferService transferService) {
        this.transferService = transferService;
    }

    @GetMapping("/getTransferForm/{transferType}")
    public String getTransferForm(Model model, @PathVariable String transferType) {
        final Transfer transfer = new Transfer();
        transfer.setState(TransferState.ACTIVE);
        model.addAttribute("transfer", transfer);
        model.addAttribute("transferType", transferType);
        return "/transfer/transfer";
    }

    @GetMapping("/getTransferType")
    public String getTransferPage() {
        return "/transfer/transferType";
    }

    @GetMapping("/history")
    public String getTransferHistory(@AuthenticationPrincipal UserDetails userDetails,
                                     Model model) {
        model.addAttribute("transfers", transferService.getTransferHistory(userDetails));
        return "/transfer/transferHistory";
    }

}
