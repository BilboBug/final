package ru.javaschool.controllers.transfer;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.javaschool.service.transferservice.BlockedTransferService;
import ru.javaschool.service.transferservice.CardToCardTransferService;

@Controller
@RequestMapping("/blockedTransfer")
public class BlockedTransferController {

    private final CardToCardTransferService transferService;
    private final BlockedTransferService blockedTransferService;

    public BlockedTransferController(CardToCardTransferService transferService, BlockedTransferService blockedTransferService) {
        this.transferService = transferService;
        this.blockedTransferService = blockedTransferService;
    }

    @GetMapping("/getBlockedTransfers")
    public String getBlockedTransfers(Model model) {
        model.addAttribute("transfers", transferService.getBlockedTransfers());
        return "transfer/blockedTransfers";
    }

    @PostMapping("/approveTransfer")
    public String approveTransfer(@RequestParam long transferId) {
        blockedTransferService.approveTransfer(transferId);
        return "redirect:/operator";
    }

    @PostMapping("/deleteTransfer")
    public String deleteTransfer(@RequestParam long transferId) {
        blockedTransferService.deleteTransfer(transferId);
        return "redirect:/operator";
    }
}
