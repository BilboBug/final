package ru.javaschool.controllers.transfer;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.javaschool.exceptions.client.ClientNotFoundException;
import ru.javaschool.models.bankentity.transfer.Transfer;
import ru.javaschool.models.users.Client;
import ru.javaschool.service.clientservice.ClientService;
import ru.javaschool.service.transferservice.TransferExecutor;

@Controller
@RequestMapping("/transfer")
public class CardToAccountTransferController {

    private final TransferExecutor transferExecutor;
    private final ClientService clientService;

    public CardToAccountTransferController(@Qualifier("cardToAccountTransferService") TransferExecutor transferExecutor, ClientService clientService) {
        this.transferExecutor = transferExecutor;
        this.clientService = clientService;
    }

    @PostMapping("/executeCardToAccountTransfer")
    public String executeTransfer(@AuthenticationPrincipal UserDetails userDetails,
                                  @ModelAttribute("transfer") Transfer transfer) {
        final Client client = clientService
                .getClientByLogin(userDetails.getUsername())
                .orElseThrow(ClientNotFoundException::new);

        transferExecutor.executeTransfer(client, transfer);
        return "redirect:/client";
    }
}
