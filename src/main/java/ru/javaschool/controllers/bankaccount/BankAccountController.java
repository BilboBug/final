package ru.javaschool.controllers.bankaccount;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.javaschool.models.bankentity.account.BankAccount;
import ru.javaschool.service.bankaccountservice.BankAccountService;
import ru.javaschool.service.clientservice.ClientService;

@Controller
@RequestMapping("/bankAccount")
public class BankAccountController {

    private final BankAccountService bankAccountService;

    @Autowired
    public BankAccountController(BankAccountService bankAccountService) {
        this.bankAccountService = bankAccountService;
    }

    @GetMapping("/getAccountForm")
    public String getAccountForm(Model model) {
        final BankAccount bankAccount = new BankAccount();
        model.addAttribute("account", bankAccount);
        return "/bankaccount/bankaccountform";
    }

    @PostMapping("/saveAccount")
    public String saveAccount(@ModelAttribute("account") BankAccount account,
                              @AuthenticationPrincipal UserDetails userDetails) {

        bankAccountService.save(account, userDetails);
        return "redirect:/client";
    }
}
