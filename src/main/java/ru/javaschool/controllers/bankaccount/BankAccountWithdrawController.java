package ru.javaschool.controllers.bankaccount;

import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.javaschool.service.bankaccountservice.BankAccountService;

@Controller
@RequestMapping("/bankAccountCash")
public class BankAccountWithdrawController {

    private final BankAccountService bankAccountService;

    public BankAccountWithdrawController(BankAccountService bankAccountService) {
        this.bankAccountService = bankAccountService;
    }

    @GetMapping("/getWithdrawForm/{bankAccountId}")
    public String getWithdrawForm(@PathVariable long bankAccountId, Model model) {
        model.addAttribute("bankAccountId", bankAccountId);
        return "/bankaccount/bankAccountWithdrawForm";
    }

    @PostMapping("/withdrawCash")
    public String withdrawCash(@AuthenticationPrincipal UserDetails userDetails,
                          @RequestParam long bankAccountId,
                          @RequestParam double amount) {

        bankAccountService.withdrawCash(userDetails, bankAccountId, amount);
        return "redirect:/client";
    }
}
