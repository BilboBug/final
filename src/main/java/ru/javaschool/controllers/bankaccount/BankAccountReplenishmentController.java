package ru.javaschool.controllers.bankaccount;

import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.javaschool.service.bankaccountservice.BankAccountService;

@Controller
@RequestMapping("/bankAccountCash")
public class BankAccountReplenishmentController {

    private final BankAccountService bankAccountService;

    public BankAccountReplenishmentController(BankAccountService bankAccountService) {
        this.bankAccountService = bankAccountService;
    }

    @GetMapping("/getPaymentForm/{bankAccountId}")
    public String getPaymentForm(@PathVariable long bankAccountId, Model model) {
        model.addAttribute("bankAccountId", bankAccountId);
        return "/bankaccount/bankAccountPayForm";
    }

    @PostMapping("/addCash")
    public String addCash(@AuthenticationPrincipal UserDetails userDetails,
                          @RequestParam long bankAccountId,
                          @RequestParam double amount) {

        bankAccountService.addCash(userDetails, bankAccountId, amount);
        return "redirect:/client";
    }
}
