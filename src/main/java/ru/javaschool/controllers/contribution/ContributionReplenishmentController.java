package ru.javaschool.controllers.contribution;

import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.javaschool.service.contributionservice.ContributionService;

@Controller
@RequestMapping("/contributionCash")
public class ContributionReplenishmentController {

    private final ContributionService contributionService;

    public ContributionReplenishmentController(ContributionService contributionService) {
        this.contributionService = contributionService;
    }

    @GetMapping("/getPaymentForm/{contributionId}")
    public String getPaymentForm(@PathVariable long contributionId, Model model) {
        model.addAttribute("contributionId", contributionId);
        return "/contribution/contributionPayForm";
    }

    @PostMapping("/addCash")
    public String addCash(@AuthenticationPrincipal UserDetails userDetails,
                          @RequestParam long contributionId,
                          @RequestParam double amount) {

        contributionService.addCash(userDetails, contributionId, amount);
        return "redirect:/client";
    }

}
