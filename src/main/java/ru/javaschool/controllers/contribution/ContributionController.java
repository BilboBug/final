package ru.javaschool.controllers.contribution;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.javaschool.service.contributionservice.ContributionService;

@Controller
@RequestMapping("/contribution")
public class ContributionController {

    private final ContributionService contributionService;

    @Autowired
    public ContributionController(ContributionService contributionService) {
        this.contributionService = contributionService;
    }

    @GetMapping("/getContributionForm")
    public String getContributionForm(Model model) {
        model.addAttribute("tariffs", contributionService.getAllTariffs());
        return "/contribution/contributionForm";
    }

    @PostMapping("/openContribution")
    public String openContribution(@AuthenticationPrincipal UserDetails userDetails,
                                   @RequestParam long tariffId,
                                   @RequestParam double amount) {

        contributionService.openContribution(userDetails, tariffId, amount);
        return "redirect:/client";
    }

    @PostMapping("/closeContribution")
    public String closeContribution(@AuthenticationPrincipal UserDetails userDetails,
                                    @RequestParam long contributionId) {
        contributionService.closeContribution(userDetails, contributionId);
        return "redirect:/client";
    }
}
