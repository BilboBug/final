package ru.javaschool.controllers.contribution;

import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.javaschool.service.contributionservice.ContributionService;

@Controller
@RequestMapping("/contributionCash")
public class ContributionWithdrawController {

    private final ContributionService contributionService;

    public ContributionWithdrawController(ContributionService contributionService) {
        this.contributionService = contributionService;
    }

    @GetMapping("/getWithdrawForm/{contributionId}")
    public String getPaymentForm(@PathVariable long contributionId, Model model) {
        model.addAttribute("contributionId", contributionId);
        return "/contribution/contributionWithdrawForm";
    }

    @PostMapping("/withdraw")
    public String withdrawCash(@AuthenticationPrincipal UserDetails userDetails,
                               @RequestParam long contributionId,
                               @RequestParam double amount) {

        contributionService.withdrawCash(userDetails, contributionId, amount);
        return "redirect:/client";
    }
}
