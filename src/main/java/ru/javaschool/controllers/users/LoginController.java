package ru.javaschool.controllers.users;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.javaschool.models.users.*;
import ru.javaschool.repositories.user.ClientRepository;
import ru.javaschool.repositories.user.GuestRepository;
import ru.javaschool.repositories.user.OperatorRepository;
import ru.javaschool.repositories.user.PassportRepository;

import java.util.Map;

@Controller
public class LoginController {

    private final PasswordEncoder passwordEncoder;

    private final GuestRepository guestRepository;

    private final ClientRepository clientRepository;

    private final OperatorRepository operatorRepository;

    private final PassportRepository passportRepository;

    @Autowired
    public LoginController(PasswordEncoder passwordEncoder, GuestRepository guestRepository, ClientRepository clientRepository, OperatorRepository operatorRepository, PassportRepository passportRepository) {
        this.passwordEncoder = passwordEncoder;
        this.guestRepository = guestRepository;
        this.clientRepository = clientRepository;
        this.operatorRepository = operatorRepository;
        this.passportRepository = passportRepository;
    }

//    @GetMapping("/login")
//    public String getLogin() {
//        return "login";
//    }

    @GetMapping("/addclient")
    public String addGuest(@RequestParam Map<String, String> params) {
        final Guest guest = new Guest();
        guest.setHashPassword(passwordEncoder.encode(params.get("password")));
        guest.setLogin(params.get("login"));
        guest.setRole(Role.CLIENT);
        guest.setState(State.ACTIVE);
        guestRepository.save(guest);
        final Passport passport = new Passport();
        passport.setFirstName(params.get("firstName"));
        passport.setLastName(params.get("lastName"));
        passport.setSerialNumber(params.get("serialNumber"));
        passportRepository.save(passport);
        final Client client = new Client();
        client.setGuest(guest);
        client.setPassport(passport);
        clientRepository.save(client);
        return "redirect:login";
    }

    @GetMapping("/addoperator")
    public String addGuest(@RequestParam String login, @RequestParam String password,
                           @RequestParam String serialNumber, @RequestParam String firstName,
                           @RequestParam String lastName) {
        final Guest guest = new Guest();
        guest.setLogin(login);
        guest.setHashPassword(passwordEncoder.encode(password));
        guest.setState(State.ACTIVE);
        guest.setRole(Role.OPERATOR);
        guestRepository.save(guest);
        final Passport passport = new Passport();
        passport.setSerialNumber(serialNumber);
        passport.setFirstName(firstName);
        passport.setLastName(lastName);
        passportRepository.save(passport);
        final Operator operator = new Operator();
        operator.setGuest(guest);
        operator.setPassport(passport);
        operatorRepository.save(operator);
        return "redirect:login";
    }
}
