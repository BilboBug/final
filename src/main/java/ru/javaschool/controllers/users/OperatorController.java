package ru.javaschool.controllers.users;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.javaschool.models.bankentity.creditcard.CreditCard;
import ru.javaschool.models.bankentity.creditcard.CreditCardApplication;
import ru.javaschool.models.bankentity.creditcard.CardState;
import ru.javaschool.models.users.Client;
import ru.javaschool.service.operatorservice.OperatorService;

import java.util.List;

@Controller
@RequestMapping("/operator")
public class OperatorController {

    private final OperatorService operatorService;

    @Autowired
    public OperatorController(OperatorService operatorService) {
        this.operatorService = operatorService;
    }

    @GetMapping
    public String getOperatorPage() {
        return "operatorcontroller/operator";
    }

    @GetMapping("/creditcardapps")
    public String getCreditCardApplications(Model model) {
        List<CreditCardApplication> applications = operatorService.getAllCreditCardApplication();
        model.addAttribute("applications", applications);
        return "operatorcontroller/creditcardapps";
    }

    @GetMapping("/creditcardapps/offer/{id}")
    public String getCreditCardOfferForm(@PathVariable long id, Model model) {
        model.addAttribute("clientId", id);
        final CreditCard card = new CreditCard();
        model.addAttribute("creditCard", card);
        return "operatorcontroller/creditcardoffer";
    }

    @PostMapping("creditcardapps/offer/save/{id}")
    public String saveCreditCardOffer(@ModelAttribute("creditCard") CreditCard creditCard, @PathVariable long id) {
        final Client client = operatorService.getClientById(id).orElseThrow(IllegalArgumentException::new);
        creditCard.setOwner(client);
        creditCard.setState(CardState.CONSIDER);
        client.getCreditCards().add(creditCard);
        operatorService.saveCreditCard(client, creditCard);
        return "redirect:/operator";
    }
}
