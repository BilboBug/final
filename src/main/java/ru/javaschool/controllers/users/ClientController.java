package ru.javaschool.controllers.users;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.javaschool.models.bankentity.creditcard.CardState;
import ru.javaschool.models.bankentity.creditcard.CreditCard;
import ru.javaschool.models.users.Client;
import ru.javaschool.service.clientservice.ClientService;

import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/client")
public class ClientController {

    private final ClientService clientService;


    @Autowired
    public ClientController(ClientService clientService) {
        this.clientService = clientService;
    }

    @GetMapping
    public String getClientPage(Model model, @AuthenticationPrincipal UserDetails userDetails) {

        Client client = clientService.getClientByLogin(userDetails.getUsername()).orElseThrow(IllegalArgumentException::new);
        final List<CreditCard> cards = client
                .getCreditCards().stream()
                .filter(val -> val.getState() != CardState.CONSIDER)
                .collect(Collectors.toList());
        model.addAttribute("name", client.getPassport().getFirstName());
        model.addAttribute("surname", client.getPassport().getLastName());
        model.addAttribute("creditcards", cards);
        model.addAttribute("bankAccounts", client.getBankAccounts());
        model.addAttribute("cards", client.getCards());
        model.addAttribute("contributions", client.getContributions());

        return "clientcontroller/client";
    }
}
