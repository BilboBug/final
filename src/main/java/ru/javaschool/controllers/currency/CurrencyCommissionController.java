package ru.javaschool.controllers.currency;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.javaschool.models.bankentity.currency.Currency;
import ru.javaschool.service.currency.CurrencyCommissionService;

@Controller
@RequestMapping("/currencyCommission")
public class CurrencyCommissionController {

    private final CurrencyCommissionService currencyCommissionService;

    public CurrencyCommissionController(CurrencyCommissionService currencyCommissionService) {
        this.currencyCommissionService = currencyCommissionService;
    }

    @GetMapping("/getCurrencyCommissionForm")
    public String getCurrencyCommissionForm() {
        return null;
    }

    @PostMapping("/setCurrencyCommission")
    public String setCurrencyCommission(@RequestParam Currency currency,
                                        @RequestParam double value) {
        currencyCommissionService.setCurrencyCommission(currency, value);
        return "redirect:/operator";
    }
}
