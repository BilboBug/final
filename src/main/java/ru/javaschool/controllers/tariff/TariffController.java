package ru.javaschool.controllers.tariff;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.javaschool.models.bankentity.contribution.Tariff;
import ru.javaschool.service.tariffservice.TariffService;

@Controller
@RequestMapping("/tariff")
public class TariffController {

    private final TariffService tariffService;

    public TariffController(TariffService tariffService) {
        this.tariffService = tariffService;
    }

    @GetMapping("/getTariffs")
    public String getTariffs(Model model) {
        model.addAttribute("tariffs", tariffService.getTariffs());
        return "/tariff/adminPanel";
    }

    @GetMapping("/getUpdateForm/{tariffId}")
    public String getUpdateTariffForm(@PathVariable long tariffId, Model model) {
        final Tariff tariff = tariffService.getTariff(tariffId);
        model.addAttribute("tariff", tariff);
        return "/tariff/tariffUpdateForm";
    }

    @GetMapping("/getCreateForm")
    public String getCreateTariffForm(Model model) {
        model.addAttribute("tariff", new Tariff());
        return "/tariff/tariffCreateForm";
    }

    @PostMapping("/updateTariff")
    public String updateTariff(@ModelAttribute("tariff")Tariff tariff) {
        tariffService.updateTariff(tariff);
        return "redirect:/operator";
    }

    @PostMapping("/createTariff")
    public String createTariff(@ModelAttribute("tariff")Tariff tariff) {
        tariffService.createTariff(tariff);
        return "redirect:/operator";
    }

    @PostMapping("/deleteTariff")
    public String deleteTariff(@RequestParam long tariffId) {
        final Tariff tariff = tariffService.getTariff(tariffId);
        tariffService.deleteTariff(tariff);
        return "redirect:/operator";
    }
}
