package ru.javaschool.controllers.card;

import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.javaschool.models.bankentity.account.Card;
import ru.javaschool.service.cardservice.CardService;

@Controller
@RequestMapping("/card")
public class CardController {

    private final CardService cardService;

    public CardController(CardService cardService) {
        this.cardService = cardService;
    }

    @GetMapping("/getCardForm")
    public String getCardForm(Model model) {
        model.addAttribute("card", new Card());
        return "/card/cardForm";
    }

    @PostMapping("/saveCard")
    public String saveCard(@AuthenticationPrincipal UserDetails userDetails,
                           @ModelAttribute("card") Card card) {
        cardService.save(card, userDetails);
        return "redirect:/client";
    }
}
