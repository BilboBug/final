package ru.javaschool.controllers.card;

import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.javaschool.service.cardservice.CardService;

@Controller
@RequestMapping("/cardCash")
public class CardWithdrawController {

    private final CardService cardService;

    public CardWithdrawController(CardService cardService) {
        this.cardService = cardService;
    }

    @GetMapping("/getWithdrawForm/{cardId}")
    public String getPaymentForm(@PathVariable long cardId, Model model) {
        model.addAttribute("cardId", cardId);
        return "/card/cardWithdrawForm";
    }

    @PostMapping("/withdraw")
    public String withdrawCash(@AuthenticationPrincipal UserDetails userDetails,
                               @RequestParam long cardId,
                               @RequestParam double amount) {

        cardService.withdrawCash(userDetails, cardId, amount);
        return "redirect:/client";
    }
}
