package ru.javaschool.controllers.credit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.javaschool.models.bankentity.credit.Credit;
import ru.javaschool.service.creditservice.OpenCreditService;

@Controller
@RequestMapping("/openCredit")
public class OpenCreditController {

    private final OpenCreditService openCreditService;

    @Autowired
    public OpenCreditController(OpenCreditService openCreditService) {
        this.openCreditService = openCreditService;
    }

    @GetMapping("/creditForm/{creditCardId}")
    public String getCreditForm(@AuthenticationPrincipal UserDetails userDetails,
                                @PathVariable long creditCardId,
                                Model model) {
        model.addAttribute("credit", openCreditService.getCreditForm(userDetails, creditCardId));
        model.addAttribute("creditCardId", creditCardId);
        return "clientcontroller/credit";
    }

    @PostMapping("/openCredit/{creditCardId}")
    public String openCredit(@AuthenticationPrincipal UserDetails userDetails,
                             @ModelAttribute("credit") Credit credit,
                             @PathVariable long creditCardId) {
        openCreditService.openCredit(userDetails, credit, creditCardId);
        return "redirect:/client";
    }


}
