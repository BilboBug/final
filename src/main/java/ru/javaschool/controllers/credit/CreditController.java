package ru.javaschool.controllers.credit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.javaschool.service.creditservice.CreditService;

@Controller
@RequestMapping("/credit")
public class CreditController {

    private final CreditService creditService;

    @Autowired
    public CreditController(CreditService creditService) {
        this.creditService = creditService;
    }

    @PostMapping("/repayCredit/{creditCardId}")
    public String repayCredit(@AuthenticationPrincipal UserDetails userDetails,
                              @RequestParam("creditId") long creditId,
                              @RequestParam double amount,
                              @PathVariable long creditCardId) {

        creditService.repayCredit(userDetails, creditId, creditCardId, amount);
        return "redirect:/client";
    }

    @GetMapping("/getCredits/{creditCardId}")
    public String getCredits(@AuthenticationPrincipal UserDetails user, @PathVariable long creditCardId, Model model) {
        model.addAttribute("credits", creditService.getCredits(user, creditCardId));
        return "/creditcard/credits";
    }

    @GetMapping("/getCreditpayForm/{creditId}")
    public String getCreditPayForm(Model model, @RequestParam long creditCardId, @PathVariable long creditId) {
        model.addAttribute("creditCardId", creditCardId);
        model.addAttribute("creditId", creditId);
        return "/credit/creditform";
    }

    @GetMapping("/getFinespayForm/{creditId}")
    public String getFinesPayForm(Model model, @RequestParam long creditCardId, @PathVariable long creditId) {
        model.addAttribute("creditCardId", creditCardId);
        model.addAttribute("creditId", creditId);
        return "/credit/finesForm";
    }

    @PostMapping("/repayFines/{creditCardId}")
    public String repayFines(@AuthenticationPrincipal UserDetails userDetails,
                             @RequestParam("creditId") long creditId,
                             @RequestParam double amount,
                             @PathVariable long creditCardId) {

        creditService.repayFines(userDetails, creditCardId, creditId, amount);
        return "redirect:/client";
    }
}
