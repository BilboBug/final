package ru.javaschool.controllers.credit;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.javaschool.service.creditservice.FinesService;

@Controller
@RequestMapping("/fines")
public class FinesController {

    private final FinesService finesService;

    public FinesController(FinesService finesService) {
        this.finesService = finesService;
    }

    @PostMapping("/setFine")
    public String setFine(@RequestParam double value) {
        finesService.setFine(value);
        return "redirect:/operator";
    }
}
