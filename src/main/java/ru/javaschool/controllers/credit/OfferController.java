package ru.javaschool.controllers.credit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.javaschool.service.offerservice.OfferService;

@Controller
@RequestMapping("/offer")
public class OfferController {

    private final OfferService offerService;

    @Autowired
    public OfferController(OfferService offerService) {
        this.offerService = offerService;
    }

    @GetMapping("/getOffers")
    public String getOffers(@AuthenticationPrincipal UserDetails userDetails, Model model) {
        model.addAttribute("offers", offerService.getOffers(userDetails));
        return "clientcontroller/offers";
    }

    @PostMapping("/acceptoffer/{offerId}")
    public String acceptOffer(@AuthenticationPrincipal UserDetails userDetails, @PathVariable long offerId) {
        offerService.acceptOffer(userDetails, offerId);
        return "redirect:/client";
    }

    @PostMapping("/denyoffer/{offerId}")
    public String denyOffer(@AuthenticationPrincipal UserDetails userDetails, @PathVariable long offerId) {
        offerService.denyOffer(userDetails, offerId);
        return "redirect:/client";
    }
}
