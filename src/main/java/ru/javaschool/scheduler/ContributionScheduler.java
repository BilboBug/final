package ru.javaschool.scheduler;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.javaschool.models.bankentity.contribution.Contribution;
import ru.javaschool.repositories.contribution.ContributionRepository;

import java.sql.Date;
import java.text.DecimalFormat;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ContributionScheduler {

    private final ContributionRepository contributionRepository;

    public ContributionScheduler(ContributionRepository contributionRepository) {
        this.contributionRepository = contributionRepository;
    }

    //@Scheduled(cron = "0 0 1 * * *")
    @Transactional
    @Scheduled(fixedDelay = 5000)
    public void setCapitalization() {
        final Date currentDate = new Date(System.currentTimeMillis());
        final List<Contribution> contributions = contributionRepository.getAllByNextCapitalizationDate(currentDate);

        final List<Contribution> list = contributions.stream()
                .filter(val -> val.getTariff().isCapitalized())
                .collect(Collectors.toList());

        for (Contribution contribution: list) {
            final double amount = contribution.getAmount() +
                    contribution.getAmount() * contribution.getTariff().getInterestRate() +
                    contribution.getReplenishmentInMonth();

            if (amount > contribution.getTariff().getMaxAmount()) {
                continue;
            }

            if (contribution.getEndDate().before(currentDate)) {
                continue;
            }

            final double parseAmount = Double
                    .parseDouble(new DecimalFormat("#0.00")
                            .format(amount).replace(',', '.'));

            contribution.setAmount(parseAmount);
            contribution.setNextCapitalizationDate(Date.valueOf(currentDate.toLocalDate().plusMonths(1)));
            contribution.setReplenishmentInMonth(0);
            contributionRepository.save(contribution);
        }
    }
}
